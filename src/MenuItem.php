<?php

namespace MatiasMuller\MenuView;

class MenuItem
{
	protected $label = '';
	protected $color = '#F00';
	protected $icon = '';
	protected $url = '';
	protected $active = '';
	protected $submenu = [];

	public function __construct($params)
	{
		if (is_array($params))
		{
			if (!empty($params['icon-class'])) {
				$this->icon = $params['icon-class'];
			}

			foreach (['label', 'color', 'url', 'active', 'submenu'] as $attr) {
				if (!empty($params[$attr])) {
					$this->$attr = $params[$attr];
				}
			}			
		}
	}

	public function label()
	{
		return $this->label;
	}

	public function color()
	{
		return $this->color;
	}

	public function icon()
	{
		return $this->icon;
	}

	public function url()
	{
		return $this->url;
	}

	public function isActive()
	{
		return $this->active;
	}

	public function submenu()
	{
		return $this->submenu;
	}

	public function hasUrl()
	{
		return $this->url <> '';
	}

	public function hasSubmenu()
	{
		return !!$this->submenu;
	}

}
