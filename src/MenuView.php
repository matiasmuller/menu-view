<?php

namespace MatiasMuller\MenuView;

class MenuView 
{
	public $level = 1;
	protected $maxLevel = 2;
	protected $itemClass = 'MatiasMuller\MenuView\MenuItem';
	protected $viewName = 'menu.lvl';

	protected $data;
	protected $realLevel;

	public function __construct($params)
	{
		if (is_array($params))
		{
			$this->data = isset($params['data']) ? $params['data'] : $params;
			$this->realLevel = isset($params['realLevel']) ? $params['realLevel'] : $this->level;

			foreach (['level', 'maxLevel', 'viewName', 'itemClass'] as $attr) {
				if (!empty($params[$attr])) {
					$this->$attr = $params[$attr];
				}
			}
		}
	}

	public function subMenu($itemData)
	{
		$menuClass = get_class($this);
		return new $menuClass([
			'data' => $itemData,
			'level' => $this->nextLevel(),
			'maxLevel' => $this->maxLevel(),
			'realLevel' => $this->nextRealLevel(),
			'viewName' => $this->viewName(),
		]);
	}

	public function maxLevel()
	{
		return $this->maxLevel;
	}

	public function realLevel()
	{
		return $this->realLevel;
	}

	public function viewName()
	{
		return $this->viewName;
	}

	public function view($n = 1)
	{
		return $this->viewName().$n;
	}

	public function nextLevel()
	{
		return ($next = $this->level + 1) > ($max = $this->maxLevel()) ? $max : $next;
	}

	public function nextRealLevel()
	{
		return $this->realLevel + 1;
	}

	public function nextView()
	{
		return $this->view($this->nextLevel());
	}

	public function hasData()
	{
		return !!$this->data;
	}
	
	public function data()
	{
		return $this->data ? array_map(function($item) {
			return new $this->itemClass($item);
		}, $this->data) : false;
	}
	
}
